import React from 'react';
import './App.css';
import Cities from './components/cities';

function App() {
  return (
    <div className="App">
      <Cities/>
    </div>
  );
}

export default App;
