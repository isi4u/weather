import { createStore } from 'redux';
import { saveState, loadState } from './localStorage';
import throttle from 'lodash.throttle';

function reducer(state = loadState(), action) {
  if(action.type === "add-city") {
    return {cities: [...state.cities, {id: state.id, ...action.payload}], id: state.id + 1}
  } else if (action.type === 'change-city') {
      return {...state, cities: [...state.cities.map( el => {
          if (el.id === action.payload.id) {
              return {...el, ...action.payload}
          } else {
              return el
          }
      })]}
  } else if (action.type === 'remove-city') {
      return {...state, cities: [...state.cities.filter( el => el.id !== action.payload.id)]}
  }
  return state;
}


const store = createStore(reducer);

store.subscribe(
  throttle( () => saveState(store.getState()), 1000)
);

export default store;
