export const saveState = (state) => {
    try  {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('cities', serializedState);
    }  catch (error) {
  
    }
}
  
  
export const loadState = () => {
    try  {
        const serializedState = localStorage.getItem('cities');
        if (serializedState === null)    {
            return {id: 1, cities: []};
        }
        return JSON.parse(serializedState);
    }  catch (error)  {
        return {id: 1, cities: []};
    }
}