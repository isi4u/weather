import React, {useState, useEffect} from 'react';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import './creator.scss'

const Creator = ({showWeather}) => {
    const cities = useSelector(state => state.cities, shallowEqual);
    const [nm, setNm] = useState('')
    const [lat, setLat] = useState(0)
    const [lng, setLng] = useState(0)
    useEffect(() => {
        try {
            navigator.geolocation.getCurrentPosition(({coords: {latitude, longitude}}) => {

                try {
                    fetch(`http://api.openweathermap.org/geo/1.0/reverse?lat=${latitude}&lon=${longitude}&limit=1&appid=bcdaf15b315c03eedaec3ae730759c0b`, {
                        method: 'GET'
                    })
                    .then((response) => {
                        return response.json();
                    })
                    .then(([{name}]) => {
                        setNm(name)
                    });
                    setLat(latitude)
                    setLng(longitude)
                    fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=bcdaf15b315c03eedaec3ae730759c0b`, {
                        method: 'GET'
                    })
                    .then((response) => {
                        return response.json();
                    })
                    .then((data) => {
                        showWeather(data)
                    });
                } catch (error) {
                    
                }

            }, error => console.warn(`ERROR(${error.code}): ${error.message}`), {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            });
        } catch (error) {
            
        }
        
      }, []);
    const dispatch = useDispatch()

    return (
        <div className="city-creator">
            <h2>New city</h2>
            <label className="city-label">City name</label>
            <input type="text" value={nm} onChange={(e) => {
                setNm(e.nativeEvent.target.value)
            }} />
            <label className="city-label">Latitude</label>
            <input type="number" value={lat} onChange={(e) => {
                setLat(e.nativeEvent.target.value)
            }} />
            <label className="city-label">Longitude</label>
            <input type="number" value={lng} onChange={(e) => {
                setLng(e.nativeEvent.target.value)
            }} />
            <button onClick={() => {
                if (cities.findIndex(el => el.cityName === nm) >= 0) {
                    alert(`The city's on the list`)
                } else {
                    dispatch({ type: 'add-city', payload: {cityName: nm, longitude: lng, latitude: lat} })
                }
                
            }}>
                Add city to the list
            </button>
        </div>
    )
}
export default Creator;