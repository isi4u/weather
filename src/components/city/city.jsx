import React from 'react';
import { useDispatch } from 'react-redux';
import './city.scss'

const City = ({id, cityName, latitude, longitude, showWeather}) => {
    const nm = cityName
    const lat = latitude
    const lng = longitude
    const dispatch = useDispatch()
    return (
        <div className="city-item">
            <input type="text" value={nm} onChange={(e) => {
                dispatch({ type: 'change-city', payload: {id: id, cityName: e.nativeEvent.target.value} })
            }} />
            <input type="number" value={lat} onChange={(e) => {
                dispatch({ type: 'change-city', payload: {id: id, latitude: e.nativeEvent.target.value} })
            }} />
            <input type="number" value={lng} onChange={(e) => {
                dispatch({ type: 'change-city', payload: {id: id, longitude: e.nativeEvent.target.value} })
            }} />
            <button onClick={() => dispatch({ type: 'remove-city', payload: {id: id} })}>
            Delete
            </button>
            <button onClick={() => {
                fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=bcdaf15b315c03eedaec3ae730759c0b`, {
                    method: 'GET'
                })
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    showWeather(data)
                });
            }}>
            Show weather
            </button>
        </div>
    )
}
export default City;