import React, {useState} from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import './cities.scss'
import City from './../city'
import Creator from './../creator'



const Cities = () => {
    const cities = useSelector(state => state.cities, shallowEqual);
    const [weather, setWeather] = useState(null)
    const showWeather = ({name, main: {temp}}) => {
        setWeather(
            <div className="weather">Temperature in {name}: {temp}</div>
        );
    };

    return (

        <>
            <h1>Cities</h1>
            
            <div className="cities-container">
                <div className="city-item">
                    <span className="header-label">City name</span>
                    <span className="header-label">Latitude</span>
                    <span className="header-label">Longitude</span>
                    <span></span><span></span>
                </div>
            {
                cities.map((item) =>
                <City showWeather={showWeather} key={item.id} {...item} />
                )
            }
            </div>
            {weather}
            <Creator showWeather={showWeather}/>

        </>

)};



export default Cities;